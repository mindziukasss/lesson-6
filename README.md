# Steps!
- `git clone https://bitbucket.org/mpacademy/hello-world.git`
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose exec php-fpm composer install`
- If you are using Linux / Docker for Mac / Docker for windows - navigate to localhost:8080
- If you are using Docker Toolbox - you need to add an entry to your /etc/hosts file with an IP to your VM. It can be found by running `docker-machine config`. Afterwards, navigate to <your-docker-host.local>:8080

# Code snippets for testing

1. File system IO speed
```php
    for ($i=0; $i<1000; $i++) {
        $file = fopen(__DIR__, "r");
    }
```

2. Network speed
```php
        for ($i=0; $i<3; $i++) {
            $file = file_get_contents("http://www.google.com");
        }
```

3. PHP execution speed
```php
        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }

```
