<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontendController extends Controller
{
    /**
     * @Route("/frontend", name="frontend")
     */
    public function index()
    {
        return $this->render('frontend/pricing.html.twig');
    }
}
