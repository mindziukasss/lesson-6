<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/product-api")
 */
class ProductAPIController extends Controller
{
    public function __construct()
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @Route("/", name="product_api_list", methods="GET")
     */
    public function list(ProductRepository $repo)
    {
        $products = $repo->findAll();

        return new Response($this->serializer->serialize($products, 'json'));
    }

    /**
     * @Route("/{id}", name="product_api_get", methods="GET")
     * @param Product $product
     */
    public function getOne(Product $product, SerializerInterface $ser)
    {
        return new Response($ser->serialize($product, 'json'));
        // return new Response($this->serializer->serialize($product, 'json'));
    }

    /**
     * @Route("/", name="product_api_add", methods="POST")
     * @param Product $product
     */
    public function addOne(Request $request, EntityManagerInterface $entityManager, SerializerInterface $ser)
    {
        $product = $ser->denormalize($request->getContent(), Product::class, 'json');
        // $product = $this->serializer->denormalize($request->getContent(), Product::class, 'json');

        // gera ideja butu validator->validate($product);
        
        $entityManager->persist($product);
        $entityManager->flush();

        return new Response($this->serializer->serialize($product, 'json'));
    }
}
