<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('price', [$this, 'formatPrice']),
        ];
    }

    /**
     * @param float|null $value
     * @return string
     */
    public function formatPrice($value, $pre = 2)
    {
        if ($value == null) {
            return '-';
        }
        return round($value, $pre).' €';
    }
}
