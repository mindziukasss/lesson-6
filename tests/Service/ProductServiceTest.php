<?php

namespace App\Tests\Service;

use App\Entity\Product;
use App\Service\ProductService;
use PHPUnit\Framework\TestCase;

class ProductServiceTest extends TestCase
{

    public function getCalcNettoPriceData()
    {
        $out = [];

        $product = new Product();
        $product->setBruttoPrice(19);
        $out[] = [
            15.01,
            $product
        ];

        return $out;
    }

    /**
     * @dataProvider getCalcNettoPriceData
     * @param float $expected
     * @param Product $product
     */
    public function testCalcNettoPrice($expected, $product)
    {
        $service = new ProductService();
        $service->calcNettoPrice($product);
        $this->assertEquals($expected, $product->getPrice());
    }
}
